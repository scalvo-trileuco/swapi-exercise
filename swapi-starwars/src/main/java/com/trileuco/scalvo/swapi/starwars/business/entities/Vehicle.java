package com.trileuco.scalvo.swapi.starwars.business.entities;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Vehicle.Builder.class)
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 6489407449359986606L;
    private String name;
    private int maxSpeed;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {

        private final String name;
        @JsonProperty("max_atmosphering_speed")
        private int maxSpeed;

        public Builder(@JsonProperty("name") String name) {
            this.name = name;
        }

        public Builder maxSpeed(int speed) {
            maxSpeed = speed;
            return this;
        }

        public Vehicle build() {
            return new Vehicle(this);
        }

    }

    public Vehicle(Builder builder) {
        name = builder.name;
        maxSpeed = builder.maxSpeed;
    }

    public String getName() {
        return name;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
