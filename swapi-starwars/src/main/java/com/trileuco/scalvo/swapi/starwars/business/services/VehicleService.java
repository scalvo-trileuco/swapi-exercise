package com.trileuco.scalvo.swapi.starwars.business.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trileuco.scalvo.swapi.starwars.business.dtos.VehicleDto;
import com.trileuco.scalvo.swapi.starwars.business.entities.Vehicle;
import com.trileuco.scalvo.swapi.starwars.business.repositories.VehicleRepository;

@Service
public class VehicleService {

    private VehicleRepository vehicleRepository;

    @Autowired
    public VehicleService(VehicleRepository vehicleRepository) {
        super();
        this.vehicleRepository = vehicleRepository;
    }

    public VehicleDto findVehiculeByUrl(String url) {
        Vehicle vehicleReponse = vehicleRepository.findByUrl(url, Vehicle.class);
        return new VehicleDto.Builder(vehicleReponse.getName()).maxSpeed(vehicleReponse.getMaxSpeed())
                .build();
    }

    public List<VehicleDto> findVehicleCatalogByUrl(List<String> urls) {
        List<Vehicle> vehicleCatalog = vehicleRepository.findListByUrl(urls, Vehicle.class);
        List<VehicleDto> vehicleDtoCatalog = new ArrayList<VehicleDto>();

        for (Vehicle itemCatalog : vehicleCatalog) {
            vehicleDtoCatalog.add(new VehicleDto.Builder(itemCatalog.getName()).maxSpeed(itemCatalog.getMaxSpeed())
                    .build());
        }
        return vehicleDtoCatalog;
    }

    public VehicleDto findFastestVehicle(List<String> urls) {
        VehicleDto fastVehicle = null;
        List<VehicleDto> vehicleCatalog = findVehicleCatalogByUrl(urls);
        if (!vehicleCatalog.isEmpty()) {
            fastVehicle = vehicleCatalog.get(0);
            for (VehicleDto itemCatalog : vehicleCatalog) {
                if (itemCatalog.getMaxSpeed() > fastVehicle.getMaxSpeed()) {
                    fastVehicle = itemCatalog;
                }
            }
        }
        return fastVehicle;
    }
}
