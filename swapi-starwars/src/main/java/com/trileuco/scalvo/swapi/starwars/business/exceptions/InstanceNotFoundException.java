package com.trileuco.scalvo.swapi.starwars.business.exceptions;

import com.trileuco.scalvo.swapi.starwars.business.services.CharacterService;

public class InstanceNotFoundException extends Exception {

    private static final long serialVersionUID = 5185866661224170400L;
    private final String message;
    private final Class<CharacterService> type;

    public InstanceNotFoundException(String string, Class<CharacterService> class1) {
        super();
        this.message = string;
        this.type = class1;
    }

    public String getId() {
        return message;
    }

    public Class<CharacterService> getType() {
        return type;
    }

}
