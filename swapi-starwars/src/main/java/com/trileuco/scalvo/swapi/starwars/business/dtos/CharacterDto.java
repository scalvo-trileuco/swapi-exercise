package com.trileuco.scalvo.swapi.starwars.business.dtos;

import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CharacterDto {

    private final String name;
    private final String birthYear;
    private final String gender;
    private final String homeworld;
    private final List<String> vehicles;
    private final List<String> films;

    public static class Builder {

        private final String name;
        private String birthYear;
        private String gender;
        private String homeworld;
        private List<String> vehicles;
        private List<String> films;

        public Builder(String name) {
            this.name = name;
        }

        public Builder birthYear(String year) {
            birthYear = year;
            return this;
        }

        public Builder gender(String characterGender) {
            gender = characterGender;
            return this;
        }

        public Builder homeworld(String home) {
            homeworld = home;
            return this;
        }

        public Builder vehicles(List<String> vehiclesCatalog) {
            vehicles = vehiclesCatalog;
            return this;
        }

        public Builder films(List<String> filmCatalog) {
            films = filmCatalog;
            return this;
        }

        public CharacterDto build() {
            return new CharacterDto(this);
        }

    }

    public CharacterDto(Builder builder) {
        name = builder.name;
        birthYear = builder.birthYear;
        gender = builder.gender;
        homeworld = builder.homeworld;
        vehicles = builder.vehicles;
        films = builder.films;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public List<String> getFilms() {
        return films;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
