package com.trileuco.scalvo.swapi.starwars.business.entities;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Film.Builder.class)
public class Film implements Serializable {

    private static final long serialVersionUID = -6301350265339925415L;
    private String title;
    private String releaseDate;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {
        private String title;
        @JsonProperty("release_date")
        private String releaseDate;

        public Builder title(String name) {
            title = name;
            return this;
        }

        public Builder releaseDate(String date) {
            releaseDate = date;
            return this;
        }

        public Film build() {
            return new Film(this);
        }

    }

    public Film(Builder builder) {
        title = builder.title;
        releaseDate = builder.releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
