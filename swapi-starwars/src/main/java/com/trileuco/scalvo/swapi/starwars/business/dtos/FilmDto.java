package com.trileuco.scalvo.swapi.starwars.business.dtos;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class FilmDto {

    private final String title;
    private final String releaseDate;

    public static class Builder {

        private final String title;
        private String releaseDate;

        public Builder(String title) {
            this.title = title;
        }

        public Builder releaseDate(String date) {
            releaseDate = date;
            return this;
        }

        public FilmDto build() {
            return new FilmDto(this);
        }

    }

    public FilmDto(Builder builder) {
        title = builder.title;
        releaseDate = builder.releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
