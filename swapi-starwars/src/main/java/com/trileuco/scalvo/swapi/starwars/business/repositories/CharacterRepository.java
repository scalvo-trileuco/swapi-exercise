package com.trileuco.scalvo.swapi.starwars.business.repositories;

import java.net.URLDecoder;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.trileuco.scalvo.swapi.starwars.business.entities.Character;
import com.trileuco.scalvo.swapi.starwars.business.entities.Person;

import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;

@Repository
public class CharacterRepository extends AbstractRepository<Person> {

    public Person findByName(String name) {
        ResponseEntity<Character> response = getRestTemplate()
                .getForEntity(URLDecoder.decode(getPeopleSearchUrl(name).toString()), Character.class);
        if (response.getBody()
                .getResults()
                .size() == 1) {
            if (response.getBody()
                    .getResults()
                    .get(0)
                    .getName()
                    .equals(name)) {
                return response.getBody()
                        .getResults()
                        .get(0);
            }
        }
        return null;
    }

    public Builder getPeopleUrlBuilder() {
        Builder builder = getBaseUrlBuilder();
        return builder.addPathSegment("people")
                .addPathSegment("");
    }

    public HttpUrl getPeopleSearchUrl(String name) {
        Builder builder = getPeopleUrlBuilder();
        return builder.addQueryParameter("search", name)
                .build();
    }

}
