package com.trileuco.scalvo.swapi.starwars.business.dtos;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class VehicleDto {

    private final String name;
    private int maxSpeed;

    public static class Builder {

        private final String name;
        private int maxSpeed;

        public Builder(String name) {
            this.name = name;
        }

        public Builder maxSpeed(int speed) {
            maxSpeed = speed;
            return this;
        }

        public VehicleDto build() {
            return new VehicleDto(this);
        }

    }

    public VehicleDto(Builder builder) {
        name = builder.name;
    }

    public String getName() {
        return name;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
