package com.trileuco.scalvo.swapi.starwars.business.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trileuco.scalvo.swapi.starwars.business.dtos.FilmDto;
import com.trileuco.scalvo.swapi.starwars.business.entities.Film;
import com.trileuco.scalvo.swapi.starwars.business.repositories.FilmRepository;

@Service
public class FilmService {

    private FilmRepository filmRepository;

    @Autowired
    public FilmService(FilmRepository filmRepository) {
        super();
        this.filmRepository = filmRepository;
    }

    public FilmDto findFilmByUrl(String url) {
        Film film = filmRepository.findByUrl(url, Film.class);

        return new FilmDto.Builder(film.getTitle()).releaseDate(film.getReleaseDate())
                .build();
    }

    public List<FilmDto> findFilmCatalogByUrl(List<String> urls) {
        List<Film> filmCatalog = filmRepository.findListByUrl(urls, Film.class);
        List<FilmDto> filmDtoCatalog = new ArrayList<FilmDto>();

        for (Film itemCatalog : filmCatalog) {
            filmDtoCatalog.add(new FilmDto.Builder(itemCatalog.getTitle()).releaseDate(itemCatalog.getReleaseDate())
                    .build());
        }
        return filmDtoCatalog;
    }

}
