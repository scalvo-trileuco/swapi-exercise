package com.trileuco.scalvo.swapi.starwars.business.services;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trileuco.scalvo.swapi.starwars.business.dtos.CharacterDto;
import com.trileuco.scalvo.swapi.starwars.business.entities.Person;
import com.trileuco.scalvo.swapi.starwars.business.exceptions.InstanceNotFoundException;
import com.trileuco.scalvo.swapi.starwars.business.repositories.CharacterRepository;

@Service
public class CharacterService {

    private CharacterRepository characterRepository;

    @Autowired
    public CharacterService(CharacterRepository characterRepository) {
        super();
        this.characterRepository = characterRepository;
    }

    public CharacterDto findCharacterByName(String name) throws InstanceNotFoundException {
        Person character = characterRepository.findByName(name);
        if (character == null) {
            throw new InstanceNotFoundException(MessageFormat.format("Character with name {0} does not exist", name),
                    CharacterService.class);
        }
        return new CharacterDto.Builder(character.getName()).birthYear(character.getBirthYear())
                .gender(character.getGender())
                .homeworld(character.getHomeworld())
                .vehicles(character.getVehicles())
                .films(character.getFilms())
                .build();
    }

}
