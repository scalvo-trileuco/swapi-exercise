package com.trileuco.scalvo.swapi.starwars.business.entities;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Planet.Builder.class)
public class Planet implements Serializable {

    private static final long serialVersionUID = -6911439542886384942L;
    private String name;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {
        private String name = "";

        public Builder name(@JsonProperty("name") String planet) {
            name = planet;
            return this;
        }

        public Planet build() {
            return new Planet(this);
        }

    }

    public Planet(Builder builder) {
        name = builder.name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
