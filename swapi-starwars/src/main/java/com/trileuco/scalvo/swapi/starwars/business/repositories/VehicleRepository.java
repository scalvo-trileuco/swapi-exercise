package com.trileuco.scalvo.swapi.starwars.business.repositories;

import org.springframework.stereotype.Repository;

import com.trileuco.scalvo.swapi.starwars.business.entities.Vehicle;

@Repository
public class VehicleRepository extends AbstractRepository<Vehicle> {

}
