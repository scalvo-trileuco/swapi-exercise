package com.trileuco.scalvo.swapi.starwars.business.entities;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Person.Builder.class)
public class Person implements Serializable {

    private static final long serialVersionUID = -4166897851344415360L;
    private String name;
    private String birthYear;
    private String gender;
    private String homeworld;
    private List<String> vehicles;
    private List<String> films;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {

        private final String name;
        @JsonProperty("birth_year")
        private String birthYear;
        private String gender;
        private String homeworld;
        private List<String> vehicles;
        private List<String> films;

        public Builder(@JsonProperty("name") String name) {
            this.name = name;
        }

        public Builder birthYear(String year) {
            birthYear = year;
            return this;
        }

        public Builder gender(String personGender) {
            gender = personGender;
            return this;
        }

        public Builder homeworld(String planet) {
            homeworld = planet;
            return this;
        }

        public Builder vehicles(List<String> vehiclesCatalog) {
            vehicles = vehiclesCatalog;
            return this;
        }

        public Builder films(List<String> movies) {
            films = movies;
            return this;
        }

        public Person build() {
            return new Person(this);
        }

    }

    public Person(Builder builder) {
        name = builder.name;
        birthYear = builder.birthYear;
        gender = builder.gender;
        homeworld = builder.homeworld;
        vehicles = builder.vehicles;
        films = builder.films;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public List<String> getFilms() {
        return films;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
