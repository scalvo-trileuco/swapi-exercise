package com.trileuco.scalvo.swapi.starwars.business.repositories;

import org.springframework.stereotype.Repository;

import com.trileuco.scalvo.swapi.starwars.business.entities.Planet;

@Repository
public class PlanetRepository extends AbstractRepository<Planet> {

}
