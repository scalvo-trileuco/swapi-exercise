package com.trileuco.scalvo.swapi.starwars.business.dtos;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PlanetDto {

    private final String name;

    public static class Builder {

        private final String name;

        public Builder(String name) {
            this.name = name;
        }

        public PlanetDto build() {
            return new PlanetDto(this);
        }

    }

    public PlanetDto(Builder builder) {
        name = builder.name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
