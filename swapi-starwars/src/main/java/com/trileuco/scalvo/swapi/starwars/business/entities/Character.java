package com.trileuco.scalvo.swapi.starwars.business.entities;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Character.Builder.class)
public class Character implements Serializable {

    private static final long serialVersionUID = -3903968390889847281L;
    private int count;
    private List<Person> results;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {

        private int count;
        private List<Person> results;

        public Builder count(int numResults) {
            count = numResults;
            return this;
        }

        public Builder results(List<Person> personCatalog) {
            results = personCatalog;
            return this;
        }

        public Character build() {
            return new Character(this);
        }

    }

    public Character(Builder builder) {
        count = builder.count;
        results = builder.results;
    }

    public int getCount() {
        return count;
    }

    public List<Person> getResults() {
        return results;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

}
