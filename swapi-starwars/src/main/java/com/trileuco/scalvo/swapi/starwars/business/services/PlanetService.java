package com.trileuco.scalvo.swapi.starwars.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trileuco.scalvo.swapi.starwars.business.dtos.PlanetDto;
import com.trileuco.scalvo.swapi.starwars.business.entities.Planet;
import com.trileuco.scalvo.swapi.starwars.business.repositories.PlanetRepository;

@Service
public class PlanetService {

    private PlanetRepository planetRepository;

    @Autowired
    public PlanetService(PlanetRepository planetRepository) {
        super();
        this.planetRepository = planetRepository;
    }

    public PlanetDto findPlanetByUrl(String url) {
        Planet planetResponse = planetRepository.findByUrl(url, Planet.class);
        return new PlanetDto.Builder(planetResponse.getName()).build();
    }

}
