package com.trileuco.scalvo.swapi.starwars.business.repositories;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.trileuco.scalvo.swapi.starwars.Constants;

import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;

@Component
public abstract class AbstractRepository<T extends Serializable> {
    private RestTemplate restTemplate;

    public <T> T findByUrl(String url, Class<T> responseType) {
        ResponseEntity<T> response = (ResponseEntity<T>) getRestTemplate().getForEntity(url, responseType);
        return response.getBody();
    }

    public List<T> findListByUrl(List<String> urls, Class<T> responseType) {
        List<T> result = new ArrayList<T>();
        for (String item : urls) {
            result.add(this.findByUrl(item, responseType));
        }
        return result;
    }

    public RestTemplate getRestTemplate() {
        if (restTemplate == null) {
            restTemplate = new RestTemplate();
        }
        return restTemplate;
    }

    public Builder getBaseUrlBuilder() {
        Builder builder = new HttpUrl.Builder().scheme(Constants.SWAPI_TRILEUCO_SCHEME)
                .host(Constants.SWAPI_TRILEUCO_HOST)
                .port(1138)
                .addPathSegment(Constants.SWAPI_TRILEUCO_PATH);
        return builder;
    }

}
