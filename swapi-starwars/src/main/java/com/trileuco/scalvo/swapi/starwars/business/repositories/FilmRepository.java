package com.trileuco.scalvo.swapi.starwars.business.repositories;

import org.springframework.stereotype.Repository;

import com.trileuco.scalvo.swapi.starwars.business.entities.Film;

@Repository
public class FilmRepository extends AbstractRepository<Film> {

}
