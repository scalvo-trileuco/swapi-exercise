package com.trileuco.scalvo.swapi.starwars;

public class Constants {
    public static final String SWAPI_TRILEUCO_HOST = "swapi.trileuco.com";
    public static final String SWAPI_TRILEUCO_SCHEME = "http";
    public static final String SWAPI_TRILEUCO_PATH = "api";

}
