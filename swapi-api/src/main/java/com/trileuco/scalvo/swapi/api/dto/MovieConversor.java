package com.trileuco.scalvo.swapi.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.trileuco.scalvo.swapi.starwars.business.dtos.FilmDto;

public class MovieConversor {

    public static List<MovieDto> toMovieDto(List<FilmDto> filmCatalog) {
        if (!filmCatalog.isEmpty()) {
            List<MovieDto> movieDtoCatalog = new ArrayList<MovieDto>();
            for (FilmDto itemCatalog : filmCatalog) {
                movieDtoCatalog.add(toMovieDto(itemCatalog));
            }
            return movieDtoCatalog;
        }
        return null;
    }

    public static MovieDto toMovieDto(FilmDto film) {
        return new MovieDto.Builder(film.getTitle()).releaseDate(film.getReleaseDate())
                .build();
    }
}
