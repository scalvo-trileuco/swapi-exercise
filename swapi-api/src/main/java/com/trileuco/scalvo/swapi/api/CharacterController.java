package com.trileuco.scalvo.swapi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trileuco.scalvo.swapi.api.dto.MovieConversor;
import com.trileuco.scalvo.swapi.api.dto.PersonDto;
import com.trileuco.scalvo.swapi.starwars.business.dtos.CharacterDto;
import com.trileuco.scalvo.swapi.starwars.business.dtos.FilmDto;
import com.trileuco.scalvo.swapi.starwars.business.dtos.VehicleDto;
import com.trileuco.scalvo.swapi.starwars.business.exceptions.InstanceNotFoundException;
import com.trileuco.scalvo.swapi.starwars.business.services.CharacterService;
import com.trileuco.scalvo.swapi.starwars.business.services.FilmService;
import com.trileuco.scalvo.swapi.starwars.business.services.PlanetService;
import com.trileuco.scalvo.swapi.starwars.business.services.VehicleService;

@RestController
public class CharacterController {

    private CharacterService characterService;
    private FilmService filmService;
    private PlanetService planetService;
    private VehicleService vehicleService;

    @Autowired
    public CharacterController(CharacterService characterService, FilmService filmService, PlanetService planetService,
            VehicleService vehicleService) {
        super();
        this.characterService = characterService;
        this.filmService = filmService;
        this.planetService = planetService;
        this.vehicleService = vehicleService;
    }

    @RequestMapping(value = "${swapi.personInfo}", method = RequestMethod.GET)
    public PersonDto doGetCharacterInfo(@RequestParam(value = "name") String name) throws InstanceNotFoundException {
        CharacterDto characterDto = characterService.findCharacterByName(name);
        VehicleDto fastestVehicle = vehicleService.findFastestVehicle(characterDto.getVehicles());
        List<FilmDto> filmCatalog = filmService.findFilmCatalogByUrl(characterDto.getFilms());
        String fastestVehicleName = fastestVehicle != null ? fastestVehicle.getName() : "n/a";

        PersonDto personDto = new PersonDto.Builder(characterDto.getName()).birthYear(characterDto.getBirthYear())
                .gender(characterDto.getGender())
                .planetName(planetService.findPlanetByUrl(characterDto.getHomeworld())
                        .getName())
                .vehicle(fastestVehicleName)
                .films(MovieConversor.toMovieDto(filmCatalog))
                .build();

        return personDto;
    }

    @ExceptionHandler(InstanceNotFoundException.class)
    public void handleInstanceNotFoundException(InstanceNotFoundException exception, HttpServletResponse response)
            throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), exception.getMessage());
    }

}