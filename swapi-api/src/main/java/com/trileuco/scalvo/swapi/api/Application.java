package com.trileuco.scalvo.swapi.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.trileuco.scalvo.swapi.api", "com.trileuco.scalvo.swapi.starwars" })
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
