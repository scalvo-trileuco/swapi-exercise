package com.trileuco.scalvo.swapi.api.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = PersonDto.Builder.class)
public class PersonDto implements Serializable {

    private static final long serialVersionUID = 7350393855273979142L;
    private final String name;
    private final String birthYear;
    private final String gender;
    private final String planetName;
    private final String vehicle;
    private final List<MovieDto> films;

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
    public static class Builder {

        private String name;
        private String birthYear;
        private String gender;
        private String planetName;
        private String vehicle;
        private List<MovieDto> films;

        public Builder(@JsonProperty("name") String name) {
            this.name = name;
        }

        public Builder birthYear(@JsonProperty("birth_year") String year) {
            birthYear = year;
            return this;
        }

        public Builder gender(String personGender) {
            gender = personGender;
            return this;
        }

        public Builder planetName(@JsonProperty("planet_name") String planet) {
            planetName = planet;
            return this;
        }

        public Builder vehicle(@JsonProperty("fastest_vehicle_driven") String personVehicle) {
            vehicle = personVehicle;
            return this;
        }

        public Builder films(List<MovieDto> movies) {
            films = movies;
            return this;
        }

        public PersonDto build() {
            return new PersonDto(this);
        }

    }

    public PersonDto(Builder builder) {
        name = builder.name;
        birthYear = builder.birthYear;
        gender = builder.gender;
        planetName = builder.planetName;
        vehicle = builder.vehicle;
        films = builder.films;
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getPlanetName() {
        return planetName;
    }

    public String getVehicle() {
        return vehicle;
    }

    public List<MovieDto> getFilms() {
        return films;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((birthYear == null) ? 0 : birthYear.hashCode());
        result = prime * result + ((films == null) ? 0 : films.hashCode());
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((planetName == null) ? 0 : planetName.hashCode());
        result = prime * result + ((vehicle == null) ? 0 : vehicle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PersonDto other = (PersonDto) obj;
        if (birthYear == null) {
            if (other.birthYear != null)
                return false;
        } else if (!birthYear.equals(other.birthYear))
            return false;
        if (films == null) {
            if (other.films != null)
                return false;
        } else if (!films.equals(other.films))
            return false;
        if (gender == null) {
            if (other.gender != null)
                return false;
        } else if (!gender.equals(other.gender))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (planetName == null) {
            if (other.planetName != null)
                return false;
        } else if (!planetName.equals(other.planetName))
            return false;
        if (vehicle == null) {
            if (other.vehicle != null)
                return false;
        } else if (!vehicle.equals(other.vehicle))
            return false;
        return true;
    }

}
