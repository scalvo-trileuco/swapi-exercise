package com.trileuco.scalvo.swapi.api;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.trileuco.scalvo.swapi.api.dto.MovieDto;
import com.trileuco.scalvo.swapi.api.dto.PersonDto;

import junit.framework.TestCase;
import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AppTest extends TestCase {

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    CharacterController characterController;

    private Builder getBaseUrlBuilder() {
        Builder builder = new HttpUrl.Builder().scheme("http")
                .host("localhost")
                .port(8080)
                .addPathSegment("swapi-proxy");
        return builder;
    }

    private Builder getPeopleUrlBuilder() {
        Builder builder = getBaseUrlBuilder();
        return builder.addPathSegment("person-info");
    }

    private HttpUrl getPeopleSearchUrl(String name) {
        Builder builder = getPeopleUrlBuilder();
        return builder.addQueryParameter("name", name)
                .build();
    }

    private PersonDto getValidPerson() {

        String name = "Luke Skywalker";
        String year = "19BBY";
        String gender = "male";
        String planet = "Tatooine";
        String vehicle = "Snowspeeder";
        String movie = "A New Hope";
        String dateMovie = "1977-05-25";

        List<MovieDto> movies = new ArrayList<MovieDto>();
        movies.add(new MovieDto.Builder(movie).releaseDate(dateMovie)
                .build());
        movies.add(new MovieDto.Builder("The Empire Strikes Back").releaseDate("1980-05-17")
                .build());
        movies.add(new MovieDto.Builder("Return of the Jedi").releaseDate("1983-05-25")
                .build());
        movies.add(new MovieDto.Builder("Revenge of the Sith").releaseDate("2005-05-19")
                .build());

        return new PersonDto.Builder(name).birthYear(year)
                .gender(gender)
                .planetName(planet)
                .vehicle(vehicle)
                .films(movies)
                .build();

    }

    @Test
    public void contextLoad() throws Exception {
        assertTrue(characterController != null);
    }

    @Test
    public void doGetPersonInfoTest() {
        PersonDto expected = getValidPerson();
        ResponseEntity<PersonDto> response = this.restTemplate
                .getForEntity(URLDecoder.decode(getPeopleSearchUrl(expected.getName()).toString()), PersonDto.class);

        PersonDto person = response.getBody();
        assertEquals(expected, person);

    }

    @Test
    public void doGetFastestVehicleTest() {
        PersonDto expected = getValidPerson();
        ResponseEntity<PersonDto> response = this.restTemplate
                .getForEntity(URLDecoder.decode(getPeopleSearchUrl(expected.getName()).toString()), PersonDto.class);

        PersonDto person = response.getBody();
        assertEquals(expected.getVehicle(), person.getVehicle());

    }

    @Test()
    public void doGetPersonInfoNotFoundTest() {
        String name = "ZZZZ";
        ResponseEntity<PersonDto> response = this.restTemplate.getForEntity(getPeopleSearchUrl(name).toString(),
                PersonDto.class);
        assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatusCodeValue());
    }

    @Test()
    public void doGetPersonInfoNotMatchTest() {
        String name = "Luke";
        ResponseEntity<PersonDto> response = this.restTemplate.getForEntity(getPeopleSearchUrl(name).toString(),
                PersonDto.class);
        assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatusCodeValue());
    }
}
