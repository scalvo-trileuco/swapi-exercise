# Swapi

APIRestFul based on SpringBoot. Swapi allows to obtain information about StarWars characters like which vehicles uses or where lives.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Download project

You can donwload the project from GitHub:
```
git clone https://bitbucket.org/scalvo-trileuco/swapi-exercise.git
```

### Installing

To install project run in project root directory :
```
mvn clean install -Dmaven.test.skip
```

Create jar packages by executing the following command :

```
mvn package -Dmaven.test.skip
```

## Running the tests

First of all for run swapi-api test you must to run Application.java. In /swapi-api/ with the command :
```
mvn spring-boot:run
```
For run all tests execute in project root directory:
```
mvn test
```

## API documentation

```
http://localhost:8080/swapi-proxy/swagger-ui.html#/
```

## Deployment

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Docker](https://docs.docker.com/) - Provides a way to run applications in a container

## Run with Docker

### Dockerfile

### Build the image

In root project directory :
```
docker build -t swapi
 ```
To see the image created :
```
docker images
```

### Run image
```
docker run -p 8080:8080 -t swapi:latest
```

